'''
.
'''

from setuptools import setup, find_packages

setup(
    name='halo_analysis',
    version='1.0',
    description="HaloAnalysis package",
    url="https://bitbucket.org/awetzel/halo_analysis",
    author='Andrew Wetzel, Shea Garrison-Kimmel',
    author_email='arwetzel@gmail.com',
    license='MIT',
    packages=find_packages(),
    zip_safe=False,
)
